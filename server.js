var http = require('http');
var express = require('express');

var port = 8000;
var app = express();
app.use(express.static(__dirname + '/src'));

var server = app.listen(port);

var io = require('socket.io')(server);

//Socket Events (Received from client)
io.on('connection', function (socket) {
    socket.emit('sayHi', socket.id);
});

console.log('[SERVER RUNNING] http://localhost:' + port);
