var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('sass', function () {
  gulp.src('./assets/sass/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./src/css'));
});

gulp.task('js', function() {
  gulp.src('./assets/js/*.js')
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./src/js'));
});

gulp.task('watch', function () {
  gulp.watch(['./assets/js/*.js'], ['js']);
  gulp.watch(['./assets/sass/*.scss'], ['sass']);
});

gulp.task('default', ['watch']);
console.log('[ASSETS BEING WATCHED]');
