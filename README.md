# Zappos Lunch App

## Getting Started:

- You'll need to install [Node](http://nodejs.org)
- From within directory, run `npm install`
- When completed, run `npm start`
- Go to `localhost:8000` in your browser
