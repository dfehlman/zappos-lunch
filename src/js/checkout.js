var clock = new Clock();

var hourUpButton = document.getElementById("hourArrowUp");
var minUpButton = document.getElementById("minArrowUp");
var amOrPMUpButton = document.getElementById("amOrPMArrowUp");
var hourDownButton = document.getElementById("hourArrowDown");
var minDownButton = document.getElementById("minArrowDown");
var amOrPMDownButton = document.getElementById("amOrPMArrowDown");
document.getElementById("timeHour").textContent = clock.hour;
document.getElementById("timeMin").textContent = clock.minute;
document.getElementById("amPm").textContent = clock.dayOrNight;

hourUpButton.onclick = function () {
    clock.increaseHour();
    document.getElementById("timeHour").textContent = clock.hour;
};

hourDownButton.onclick = function () {
    clock.decreaseHour();
    document.getElementById("timeHour").textContent = clock.hour;
};

minUpButton.onclick = function () {
    clock.increaseMin();
    document.getElementById("timeMin").textContent = clock.minute;
};

minDownButton.onclick = function () {
    clock.decreaseMin();
    document.getElementById("timeMin").textContent = clock.minute;
};

amOrPMUpButton.onclick = function () {
    clock.amOrPM();
    document.getElementById("amPm").textContent = clock.dayOrNight;
};

amOrPMDownButton.onclick = function () {
    clock.amOrPM();
    document.getElementById("amPm").textContent = clock.dayOrNight;
};







