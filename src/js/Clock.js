function Clock() {
    this.hour = "12";
    this.minute = "00";
    this.dayOrNight = "PM";

    this.increaseHour = function () {
        if (parseInt(this.hour) == 12) {
            this.hour = "1";
        }
        else {
            this.hour = String(parseInt(this.hour) + 1);
        }
        if (parseInt(this.hour) >= 1 && parseInt(this.hour) <= 9) {
            this.hour = "0" + this.hour;
        }
    };

    this.decreaseHour = function () {
        if (parseInt(this.hour) == 1) {
            this.hour = "12";
        }
        else {
            this.hour = String(parseInt(this.hour) - 1);
        }
        if (parseInt(this.hour) >= 1 && parseInt(this.hour) <= 9) {
            this.hour = "0" + this.hour;
        }
    };

    this.increaseMin = function () {
        if (parseInt(this.minute) == 45) {
            this.minute = "00";
        }
        else {
            this.minute = String(parseInt(this.minute) + 15);
        }
    };

    this.decreaseMin = function () {
        if (parseInt(this.minute) == 0) {
            this.minute = "45";
        }
        else {
            this.minute = String(parseInt(this.minute) - 15);
        }
        if (parseInt(this.minute) == 0) {
            this.minute = "0" + this.minute;
        }
    };

    this.amOrPM = function () {
        if (this.dayOrNight == "PM") {
            this.dayOrNight = "AM";
        }
        else {
            this.dayOrNight = "PM";
        }
    }
}